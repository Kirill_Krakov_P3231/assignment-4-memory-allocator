#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

int success_count = 0;

/*Обычное успешное выделение памяти.*/
void test_simple_allocate() {
    printf("Test #1: Simple memory allocation\n");
    void* simple_malloc = _malloc(100);
    if (!simple_malloc) {
        printf("Test failed\n");
    } else {
        printf("Test passed\n");
        success_count++;
    }
    heap_term();
}

/*Освобождение одного блока из нескольких выделенных.*/
void test_releasing_one_block() {
    printf("Test #2: Releasing one block\n");
    void* first_pointer = _malloc(10);
    void* second_pointer = _malloc(10);
    _free(second_pointer);
    struct block_header* first_block = (struct block_header*) (first_pointer - offsetof(struct block_header, contents));
    struct block_header* second_block = (struct block_header*) (second_pointer - offsetof(struct block_header, contents));
    if (!second_block->is_free || first_block->next != second_block) {
        printf("Test failed\n");
    } else {
        printf("Test passed\n");
        success_count++;
    }
    heap_term();
}

/*Освобождение двух блоков из нескольких выделенных.*/
void test_releasing_two_blocks() {
    printf("Test #3: Releasing two blocks\n");
    void* first_pointer = _malloc(10);
    void* second_pointer = _malloc(10);
    _free(first_pointer);
    _free(second_pointer);
    struct block_header* first_block = (struct block_header*) (first_pointer - offsetof(struct block_header, contents));
    struct block_header* second_block = (struct block_header*) (second_pointer - offsetof(struct block_header, contents));
    if (!second_block->is_free || !first_block->is_free || first_block->next != second_block) {
        printf("Test failed\n");
    } else {
        printf("Test passed\n");
        success_count++;
    }
    heap_term();
}

/*Память закончилась, новый регион памяти расширяет старый.*/
void test_expansion_of_old_region() {
    printf("Test #4: End of memory. New memory region expands the old one\n");
    heap_init(100);
    void *pointer = _malloc(200);
    if (HEAP_START + offsetof(struct block_header, contents) != pointer) {
        printf("Test failed\n");
    } else {
        printf("Test passed\n");
        success_count++;
    }
    heap_term();
}

/*Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
Тесты должны запускаться из main.c, но могут быть описаны в отдельном (отдельных) файлах.
Алгоритм не самый простой, легко ошибиться. Чтобы не тратить времени на отладку, обязательно делайте разбиение на маленькие функции!*/
void test_new_region_in_different_place() {
    printf("Test #5: End of memory. It isn't possible to expands the old memory region. New mone in different place\n");
    void* pointer = heap_init(0);
    struct block_header* block = pointer;
    void* data = mmap(pointer+REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    _malloc(REGION_MIN_SIZE + 2);
    if (!data || !block->is_free) {
        printf("Test failed\n");
    } else {
        printf("Test passed\n");
        success_count++;
    }
    heap_term();
}

/*Все тесты*/
int main() {
    printf("Start tests:\n");
    test_simple_allocate();
    test_releasing_one_block();
    test_releasing_two_blocks();
    test_expansion_of_old_region();
    test_new_region_in_different_place();
    printf("Count of passed tests: %d out of 5",success_count);
    return 0;
}
