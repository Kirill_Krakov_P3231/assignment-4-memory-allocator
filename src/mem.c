#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t region_size = region_actual_size(offsetof(struct block_header, contents) + query); // определяем необходимый размер региона
  void* region_address = map_pages(addr,region_size,MAP_FIXED); // отражаем в память (используем addr), получаем указатель на область региона
  if (region_address == MAP_FAILED) region_address = map_pages(addr, region_size, 0); //если при отражении в память возникла ошибка, то снова пробуем отражать в память (использование addr не обязательно)
  if (region_address == MAP_FAILED) return (struct region){0}; // если снова ошибка, то возвращаем "пустой" регион
  block_init(region_address, (block_size) {region_size}, NULL); // инициализируем блок в регионе (через создание структуры его заголовка)
  return (struct region) { //возвращаем структуру выделенного региона
      region_address,
      region_size,
      (region_address == addr),
  };
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой)--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block_splittable(block, query)) return false; //если блок нельзя разделить, то возвращаем false
  query = size_max(query, BLOCK_MIN_CAPACITY); //определяем вместимость выделяемого нового блока (n)
  void* new_address = block->contents + query; //определяем адрес оставшейся части старого большого блока
  block_size new_size = (block_size) {block->capacity.bytes - query}; //определяем размер оставшейся части старого большого блока
  block_init(new_address,new_size, block->next); //инициализируем блок из этой оставшейся части
  block->capacity.bytes = query; // для выделяемого нового блока (n) указываем его вместимость
  block->next = new_address; // для выделяемого нового блока (n) указываем ссылку на следующий блок (оставшуюся частьь старого)
  return true; //возвращаем true
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    struct block_header* block_addr = HEAP_START; //определяем указатель на первый блок в куче (потом будет меняться на текущий блок)
    void* start = block_addr;//определяем адрес начала кучи
    while(block_addr) { //пока адрес блока в куче не NULL:
        size_t length = 0; //length = 0
        while (block_addr->next && blocks_continuous(block_addr, block_addr->next)) { //пока у блока есть следующий, который идет сразу за ним
            length += size_from_capacity(block_addr->capacity).bytes; // добавляем к length размер блока, исходя из его вместимости
            block_addr = block_addr->next; // указываем на следующий блок в куче
        }
        if (block_addr) { //если последний блок существует, то:
            length += size_from_capacity(block_addr->capacity).bytes; // добавляем к length размер последнего подходящего блока
            block_addr = block_addr->next; // указываем на следующий блок в куче (не идет сразу за предыдущим или NULL)
        }
        munmap(start, length); // снимаем отражение из памяти "пройденных" в этой итерации цикла блоков из кучи
        start = block_addr; //переопределяем адрес начала кучи
    }
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (!(block->next && mergeable(block, block->next))) return false; //если нет следующего блока, или их нельзя объединить, возвращаем false
    block_init(block, (block_size) {size_from_capacity(block->capacity).bytes + //создаём новый блок, объединённый из *block и block->next
            size_from_capacity(block->next->capacity).bytes}, block->next->next);
    return true; // возвращаем true
}

/*  --- ... еcли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    if (!block) {
        return (struct block_search_result) {BSR_CORRUPTED, NULL}; // если block == NULL, то возвращаем результат типа BSR_CORRUPTED
    }
    struct block_header* last; //объявляем указатель на последний "проверенный" блок
    while (block) { // пока block != NULL, то:
        while (try_merge_with_next(block)); // пробуем объединить в наиболее крупный блок
        if (block->is_free && block->capacity.bytes >= sz) { //если полученный блок свободен и его вместимость больше sz, то:
            return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK,block}; //возвращаем результат типа BSR_FOUND_GOOD_BLOCK со ссылкой на получившийся блок
        }
        last = block; //переопределяем указатель на последний "проверенный" блок
        block = block->next; // указываем на следующий блок в куче
    }
    return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, last}; //если "проверена" вся куча и подходящий блок не найден, то возвращаем результат типа BSR_REACHED_END_NOT_FOUND со ссылкой на последний блок
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result search_result = find_good_or_last(block, query); //проверяем нашу кучу, находим либо подходящий блок, либо последний блок, либо NULL
  if (search_result.type != BSR_FOUND_GOOD_BLOCK) return search_result; // если не нашли подходящий блок, то возвращаем полученный при проверке результат
  split_if_too_big(search_result.block, query); // разделяем блок, если он слишком большой по вместимости (т.е. его можно разделить)
  search_result.block->is_free = false; //указываем, что получившийся блок занят
  return search_result; //возвращаем полученный результат
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  struct region region = alloc_region(block_after(last), query); //аллоцируем регион памяти вместимостью query и по указателю после последнего блока
  if (region_is_invalid(&region)) { //если адрес получившегося региона == NULL, то:
      return NULL; //возвращаем NULL
  }
  last->next = region.addr; //определяем ссылку на следующий блок у последнего блока как адрес аллоцированного региона памяти
  return try_merge_with_next(last) ? last : last->next; //если можно объединить последний блок и следующий ему блок, то возвращаем указатель на последний блок, иначе на следующий ему (т.е. указатель на блок в аллоцированном регионе)
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (!heap_start) { //если адрес начала кучи равен NULL, то:
      return NULL; //возвращаем NULL
  }
  query = size_max(query, BLOCK_MIN_CAPACITY); //определяем вместимость
  struct block_search_result search_result = try_memalloc_existing(query,heap_start); //пробуем выделить память в куче без ее расширения
  switch (search_result.type) { //проверяем получившийся результат
      case BSR_CORRUPTED: //если получили результат типа BSR_CORRUPTED, то:
        return NULL; //возвращаем NULL
      case BSR_FOUND_GOOD_BLOCK: //если получили результат типа BSR_FOUND_GOOD_BLOCK, то:
        return search_result.block; //возвращаем указатель на выделенный блок (его вместимость >= query)
      case BSR_REACHED_END_NOT_FOUND: //если получили результат типа BSR_REACHED_END_NOT_FOUND, то:
        search_result = try_memalloc_existing(query, grow_heap(search_result.block,query)); //пробуем выделить память в расширенной куче
        if (search_result.type == BSR_FOUND_GOOD_BLOCK) return search_result.block; // если получили результат типа BSR_FOUND_GOOD_BLOCK, то возвращаем указатель на выделенный блок (его вместимость >= query)
        else return NULL; // иначе возвращаем NULL
  }
  return NULL; //возвращаем NULL
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  try_merge_with_next(header); //пробуем объединить блок с заголовком header со следующим блоком
}
